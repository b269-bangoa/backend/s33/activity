//console.log("Hi");


//GET method: 

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

//MAP method
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(titleArr => {
   console.table(titleArr.map((body) => { return body.title}))}
);


//GET method - single item

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
})
.then((response) => response.json())
//STATUS and TITLE
.then((json) => console.log(`The item "${json.title}"" on the list has a status of "${json.completed}" script.`));

//POST Method
fetch('https://jsonplaceholder.typicode.com/posts/', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
	    title: "Created To Do List Item",
	    completed: false
	})
})
.then((response) => response.json())
.then((json) => console.table(json));

//PUT Method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
	    description: "To update the my to do list with a different data structure",
	    status: "Pending",
	    title: "Update To Do List Item",
	    userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.table(json));


//PATCH Method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false
		dateCompleted: "07/09/21",
	   status: "Complete",
	   title: "delectus aut autem",
	   userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.table(json));


//DELETE Method
fetch('https://jsonplaceholder.typicode.com/posts/1', { method: 'DELETE' 
});
